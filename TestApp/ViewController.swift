//
//  ViewController.swift
//  TestApp
//
//  Created by Zamiul Haque on 2019-07-03.
//  Copyright © 2019 Zamiul Haque. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var parseButton: UIButton!
    @IBOutlet weak var wordField: UITextField!
    
    let wordsNotFoundMessage = "Could not find any english words!"
    let dictionary = EnglishDictionary.instance
    
    @IBAction func didTapParse() {
        if let wordString = wordField.text, let wordsFound = dictionary?.findWords(inString: wordString), wordsFound.count > 0 {
            resultLabel.text = wordsFound.joined(separator: " ")
            
        } else {
            resultLabel.text = wordsNotFoundMessage
            
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

