//
//  EnglishDictionary.swift
//  TestApp
//
//  Created by Zamiul Haque on 2019-07-03.
//  Copyright © 2019 Zamiul Haque. All rights reserved.
//

import Foundation

class EnglishDictionary {
    
    static let instance: EnglishDictionary! = EnglishDictionary()
    
    let englishDictionaryFileName = "engmix"
    var words = [ String ]()
    
    init?() {
        guard let dictionaryPath = Bundle.main.path(forResource: englishDictionaryFileName, ofType: "txt") else {
            print("Could not find \(englishDictionaryFileName).txt in main bundle!")
            return nil
            
        }
        
        do {
            let textFileContents = try String(contentsOfFile: dictionaryPath)
            words = textFileContents.components(separatedBy: "\n")
            
        } catch {
            print(error)
            return nil
            
        }
        
    }
    
    func findWords(inString string: String) -> [String] {
        var wordsMatched = [ String ]()
        
        for word in words {
            if string.lowercased().contains(word) {
                wordsMatched.append(word)
                
            }
            
        }
        return wordsMatched
    
    }
}
